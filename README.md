# dophon-db

#### 项目介绍
dophon框架的数据库模块,也可单独作为一个与数据库模块交互的部件

支持mysql数据库连接

支持orm形式的数据操作

如有疑问请发送邮件联系作者:ealohu@163.com

#### 软件架构

模块架构分为以组件:

1. mysql连接组件(包括连接池,连接封装类,分页,结果输出过滤器,远程接收器,增量检测)

2. orm映射组件(包括映射基础结构定义,映射封装工具,映射操作定义)

3. 抽象工具集(包括结果集文件读取,解析,热更新,动态赋值等)

4. 多数据源(xml,orm)管理器,已内嵌到相应模块中

#### 安装教程

pip安装:

```commandline
pip install dophon-db [--user]
```

#### 使用说明

##### 0. 配置相关


```python
# 此处为数据库配置
pool_conn_num = 5 # size of db connect pool() # 数据库连接池连接数(默认5个)
pydc_host = 'localhost' # 数据库连接地址
pydc_port = 3306 # 数据库连接端口
pydc_user = 'username' # 数据库连接用户名
pydc_password = 'password' # 数据库连接密码
pydc_database = 'database' # 连接数据库名(可在后面跟连接参数)
pydc_xmlupdate_sech = False # 结果集映射调度开关
db_pool_exe_time = False # 连接池执行时间调试开关

# 多数据源配置(数据库表创建请看test/test.sql)
db_cluster = [
    {
        'alias': 'data-a',
        'host': 'localhost',
        'port': 3306,
        'database': 'test1',
        'user': 'root',
        'password': 'root'
    }, {
        'alias': 'data-b',
        'host': 'localhost',
        'port': 3306,
        'database': 'test2',
        'user': 'root',
        'password': 'root'
    }, {
        'alias': 'data-c',
        'host': 'localhost',
        'port': 3306,
        'database': 'test3',
        'user': 'root',
        'password': 'root',
        'tables':['a','user']
    }, {
        'alias': 'data-d',
        'host': 'localhost',
        'port': 3306,
        'database': 'test4',
        'user': 'root',
        'password': 'root'
    }
]


```


---

##### 1. 结果集映射方式

结果集:sql执行脚本的一个集合,由于在实际开发中查询居多,简称结果集

> 通过xml文件规范若干结果集组成
```xml
<!--test.xml-->
<select id="findAll">
    SELECT
    *
    FROM
    table
</select>
```

> 通过代码关联xml文件,初始化结果集

```python
from dophon.mysql import *

_cursor=db_obj('/test.xml',auto_fix=True)

# 根路径为配置文件路径
# 文件路径必须以/开头

```

> 通过代码获取xml文件其中某一个结果集(以id区分)

```python
result= _cursor.exe_sql(methodName='findAll')
```
> 支持动态参数传入(#{}形式)以及骨架参数传入(${形式})

动态参数传入:
```xml
<select id="findAllById">
    SELECT
    *
    FROM
    table
    WHERE
    id=#{id}
</select>
```

```python
result= _cursor.exe_sql(methodName='findAllById',args={'id':'12345678'})
```

骨架参数传入:
```xml
<select id="findAllByTableName">
    SELECT
    *
    FROM
    ${table_name}
</select>
```

```python
result= _cursor.exe_sql(methodName='findAllByTableName',args={'table_name':'test_table'})
```

> 支持单条查询,列表查询,队列查询(结果集id与参数列表的列表形式和字典形式)

单条查询:
```xml
<select id="findAllById">
    SELECT
    *
    FROM
    table
    WHERE
    id=#{id}
</select>
```
```python
result= _cursor.exe_sql_single(methodName='findAllById',args={'id':'12345678'})


# result<dict>

```

列表查询:
```xml
<select id="findAllById">
    SELECT
    *
    FROM
    table
    WHERE
    id=#{id}
</select>
```
```python
result= _cursor.exe_sql(methodName='findAllById',args={'id':'12345678'})

# result<list>

```

队列查询:

1.列表形式:
```python
result= _cursor.exe_sql_queue(
    method_queue=['test1','test2'],
    args_queue=[
        {'id':'123456','name':'tom'},
        {}
    ]
)

# result<dict>
# {
#   method_name:exec_result
# }

```
2.字典形式:
```python
result= _cursor.exe_sql_obj_queue(
    queue_obj={
        'test1':{
            'id':'123456'
        },
        'test2':{}
    }
)

# result<dict>
# {
#   method_name:exec_result
# }

```

> 支持结果集文件热更新

```python
update_round(_cursor,1)

# update_round(<cursor>,second:int)

```

> 支持远程维护结果集文件

```python
# remote_path为xml文件下载地址
remote_cell = remote.getCell('test.xml', remote_path='http://127.0.0.1:8400/member/export/xml/test.xml')
_cursor = db_obj(remote_cell.getPath(), debug=True)
# 或者
_cursor = db_obj(remote_cell, debug=True)
```

---

##### 2. 表模型映射方式

暂时支持单条事务操作

> 通过初始化模型管理器获取数据库表映射骨架

```python
from dophon import orm
manager = orm.init_orm_manager(['user'])
```
> 通过实例化映射骨架获取表操作缓存实例(操作实例)

```python
user = manager.user()
```
> 通过对操作实例赋值进行对对应表模拟操作


```python
print('打印对象变量域')
for attr in dir(user):
    print(attr, ":", eval("user." + attr))
print('开始对对象赋值')
user.user_id = 'id'
user.info_id = 'info_id'
user.user_name = 'user_name'
user.user_pwd = 'user_pwd'
user.user_status = 123
user.create_time = datetime.datetime.now().strftime('%y-%m-%d')
user.update_time = datetime.datetime.now().strftime('%y-%m-%d')
print('对象赋值完毕')
print('打印对象变量域')
for attr in dir(user):
    print(attr, ":", eval("user." + attr))
print('打印对象参数表')
print(user([]))

print('user([]):', user([]))
print("user(['user_id','info_id']):", user(['user_id', 'info_id']))
print("user.get_field_list():", user.get_field_list())
print("user.alias('user_table').get_field_list():", user.alias('user_table').get_field_list())



```
> 通过对操作实例结构化操作对数据库对应表结构进行数据落地操作

```python
# 打印对象操作语句(内部方法)
print(user.where())
print(user.values())

user.select()  # 执行对象查询操作(未赋值对象执行全部查询)
user.user_name = '111' # 对对象某一属性赋值
user.select_one() # 执行单条条件查询(条件为对象已有值),复数结果时抛出异常
user.select_all() # 执行全部条件查询(条件为对象已有值)

user = manager.user() # 获取另一个模型对象
user.alias('u').select() # 对对象赋予别名并执行全部查询操作
user.user_name = '111' # 对对象某一属性赋值
user.alias('us').select_one() # 对对象赋予另一个别名并执行全部查询操作
user.alias('userr').select_all() # 对对象赋予另一个别名并执行全部查询操作

# 对对象某一属性赋值(属性类型与数据库类型相对应)
user.user_id='test_id' # 字符串类型
user.info_id='test_info_id'
user.user_name='test_user_name'
user.user_pwd='test_user_pwd'
user.user_status=1 # 数字类型
user.create_time = datetime.datetime.now().strftime('%y-%m-%d') # 日期类型
user.update_time = datetime.datetime.now().strftime('%y-%m-%d')

print(user.insert()) # 执行对象插入操作并打印操作结果

# 对对象某一属性赋值并选择其中某部分属性进行更新,其中指定了执行更新查询条件
user.user_id = 'test_id'
user.info_id = 'info_id'
user.user_name = '柯李艺'
user.user_pwd = '333'
user.user_status = 123
print(user.update(update=['user_name','user_pwd'],where=['user_id']))

# 对对象某一属性赋值并指定删除条件进行删除操作
user.user_id = 'test_id'
user.info_id = 'info_id'
user.user_name = 'user_name'
user.user_pwd = 'user_pwd'
user.user_status = 123
print(user.delete(where=['user_id']))

# 获取两个新的模型对象
user1=manager.user()
user2=manager.user()
# 打印对象1的全部查询结果
print(user1.select())
# 对对象1属性赋值
user1.user_name='early'
# 执行对象1左关联对象2,并指定关联关系(user1.user_id = user2.user_id)
user1.left_join(user2,['user_id'],['user_id'])
# 执行对象1左关联对象2,指定对象1别名(u1),对象2别名(u2),并指定关联关系(user1.user_id = user2.user_id)
user1.alias('u1').left_join(user2.alias('u2'),['user_id'],['user_id'])
# print(user1.exe_join())
# 打印对象1关联后的全部查询结果
print(user1.select())

"""
模型对象的复制与生成
"""
# 获取新的模型操作对象
user1 = manager.user()
print('user1', '---', id(user1)) # 打印对象1的id
user2 = user1.copy_to_obj(manager.user) # 利用对象管理器实例中的模型模板进行对象获取
print('user2', '---', id(user2)) # 打印对象2的id
print(user1('user_id'))
user3 = user1.read_from_dict({
    'user_id': '111'
})  # 利用模型对象中的翻译方法将字典生成一个新的模型对象
print('user3', '---', id(user3)) # 打印对象3的id
# 对比两者id
print(user1('user_id')) 
print(user3('user_id'))
```


#### 参与贡献


若有意向参与贡献,请留言或发送邮件至ealohu@163.com

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
