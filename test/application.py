import os

blueprint_path = ['/routes']
db_pool_exe_time = False
debug_trace = False
error_info = 'HTML'
host = '127.0.0.1'
ip_count = False
mq = {'remote_center': False}
msg_queue_debug = False
msg_queue_max_num = 30
pool_conn_num = 5
port = 443
project_root = os.getcwd()
pydc_database = 'test3'
pydc_host = 'localhost'
pydc_password = 'root'
pydc_port = 3306
pydc_user = 'root'
pydc_xmlupdate_sech = False
db_mode = 'CLUSTER'  # 也支持int形式赋值
server_gevented = False
server_processes = 1
server_threaded = False
ssl_context = 'adhoc'
db_cluster = [
    {
        'alias': 'data-a',
        'host': 'localhost',
        'port': 3306,
        'database': 'test1',
        'user': 'root',
        'password': 'root'
    }, {
        'alias': 'data-b',
        'host': 'localhost',
        'port': 3306,
        'database': 'test2',
        'user': 'root',
        'password': 'root'
    }, {
        'alias': 'data-c',
        'host': 'localhost',
        'port': 3306,
        'database': 'test3',
        'user': 'root',
        'password': 'root',
        'tables': ['a', 'user']
    }, {
        'alias': 'data-d',
        'host': 'localhost',
        'port': 3306,
        'database': 'test4',
        'user': 'root',
        'password': 'root'
    }
]

sqlite = {
    'db1': {
        'database': 'test.db'
    }
}
