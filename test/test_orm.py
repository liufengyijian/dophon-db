from dophon_db.orm import *
import datetime


def mark_exe_times(f, *args, **kwargs):
    """
    显示执行时间
    :param f:
    :param args:
    :param kwargs:
    :return:
    """

    def mark():
        s_time = datetime.datetime.now().timestamp()
        result = f(*args, **kwargs)
        e_time = datetime.datetime.now().timestamp()
        print('耗时:', str((e_time - s_time)), '秒', '<', s_time, ',', e_time, '>')
        return result

    return mark


@mark_exe_times
def single_manager_test_home():
    """
    随意测试

    第一次测试:(耗时: 5.209961175918579 秒 < 1544171282.966796 , 1544171288.176757 >)
    第二次测试:(耗时: 49.18696713447571 秒 < 1545807532.423368 , 1545807581.610335 >)(user表已有577385数据)
    第三次测试:(耗时: 64.39596104621887 秒 < 1546010227.63304 , 1546010292.029001 >>)(user表已有577385数据)
    :return:
    """
    print('初始化orm管理器')
    manager = init_orm(['user'])
    print('获取orm管理器中对应表映射对象')
    user = manager.user()

    user.user_id = '111'
    print(user.user_id)
    user.flush()
    print(user.user_id)
    user.user_id = '222'
    print(user.user_id)
    user.flush()
    print(user.user_id)
    user.user_id = '333'
    print(user.user_id)
    user.flush()
    print(user.user_id)
    user.user_id = '444'
    print(user.user_id)
    user.flush()
    print(user.user_id)
    user.user_id = '555'
    print(user.user_id)
    user.flush()
    print(user.user_id)

    user.user_id = '666'
    print(user.user_id)
    user.flush()
    print(user.user_id)
    user.user_id = '777'
    print(user.user_id)
    user.flush()
    print(user.user_id)
    user.user_id = '888'
    print(user.user_id)
    user.flush()
    print(user.user_id)

    print('打印对象变量域')
    for attr in dir(user):
        print(attr, ":", eval("user." + attr))
    print('开始对对象赋值')
    user.user_id = 'id'
    user.info_id = 'info_id'
    user.user_name = 'user_name'
    user.user_pwd = 'user_pwd'
    user.user_status = 123
    user.create_time = datetime.datetime.now()
    # user.create_time = datetime.datetime.now().strftime('%y-%m-%d')
    # user.update_time = datetime.datetime.now().strftime('%y-%m-%d')
    user.update_time = datetime.datetime.now()
    print('对象赋值完毕')
    print('打印对象变量域')
    for attr in dir(user):
        print(attr, ":", eval("user." + attr))
    print('打印对象参数表')
    print(user([]))

    print('user([]):', user([]))
    print("user(['user_id','info_id']):", user(['user_id', 'info_id']))
    print("user.get_field_list():", user.get_field_list())
    print("user.alias('user_table').get_field_list():", user.alias('user_table').get_field_list())

    print(user.where())
    print(user.values())

    # user.select()
    user.limit(0, 100).select()
    user.user_name = '111'
    user.select_one()
    user.limit(0, 100).select_all()

    user = manager.user()
    user.alias('u').limit(0, 100).select()
    user.user_name = '111'
    user.alias('us').limit(0, 100).select_one()
    user.alias('userr').limit(0, 100).select_all()

    user.user_id = 'test_id'
    user.info_id = 'test_info_id'
    user.user_name = 'test_user_name'
    user.user_pwd = 'test_user_pwd'
    user.user_status = 1
    user.create_time = datetime.datetime.now().strftime('%y-%m-%d')
    user.update_time = datetime.datetime.now().strftime('%y-%m-%d')

    print(user.insert())
    #
    user.user_id = 'test_id'
    user.info_id = 'info_id'
    user.user_name = '柯李艺'
    user.user_pwd = '333'
    user.user_status = 123
    print(user.update(update=['user_name', 'user_pwd'], where=['user_id']))
    #
    user.user_id = 'test_id'
    user.info_id = 'info_id'
    user.user_name = 'user_name'
    user.user_pwd = 'user_pwd'
    user.user_status = 123
    print(user.delete(where=['user_id']))

    user1 = manager.user()
    user2 = manager.user()
    print(user1.limit(0, 100).select())
    user1.user_name = 'early'
    user1.left_join(user2, ['user_id'], ['user_id'])
    user1.alias('u1').left_join(user2.alias('u2'), ['user_id'], ['user_id'])
    # print(user1.exe_join())
    print(user1.limit(0, 100).select())

    user1 = manager.user()
    print('user1', '---', id(user1))
    user2 = user1.copy_to_obj(manager.user)
    print('user2', '---', id(user2))
    print(user1('user_id'))
    print(user1.user_id)
    user3 = user1.read_from_dict({
        'user_id': '111'
    })
    print('user3', '---', id(user3))
    print(user1('user_id'))
    print(user1.user_id)
    print(user3('user_id'))
    count_user = manager.user()
    print(count_user.count('user_id'))


@mark_exe_times
def test_seq_insert():
    """
    测试批量新增

    连接池容量过大反而导致效率降低

    容量为5:147s
    容量为10:149s

    第一次测试:(耗时: 100.29046106338501 秒 < 1545321968.127197 , 1545322068.417658 >)
    第二次测试:(耗时: 1.2169852256774902 秒 < 1545623110.167153 , 1545623111.384138 >)(100)
    第三次测试:(耗时: 2.0180368423461914 秒 < 1545629565.497919 , 1545629567.515956 >)(10000)(ThreadPoolExecutor)
    第四次测试:(耗时: 6.04200005531311 秒 < 1545629604.50492 , 1545629610.54692 >)(10000)(Thread)
    第五次测试:(耗时: 3.8210291862487793 秒 < 1545629677.577921 , 1545629681.39895 >)(10000)(ProcessPoolExecutor)
    第六次测试:(耗时: 71.62900018692017 秒 < 1545629956.677919 , 1545630028.306919 >)(10000)(sequence)

    ps.使用线程池最好平衡于系统资源创建消耗与语句执行效率

    :return:
    """
    import uuid
    import random
    manager = init_orm(['user'])
    # 测试批量新增
    last_names = ['赵', '钱', '孙', '李', '周', '吴', '郑', '王', '冯', '陈', '褚', '卫', '蒋', '沈', '韩', '杨', '朱', '秦', '尤', '许',
                  '姚', '邵', '堪', '汪', '祁', '毛', '禹', '狄', '米', '贝', '明', '臧', '计', '伏', '成', '戴', '谈', '宋', '茅', '庞',
                  '熊', '纪', '舒', '屈', '项', '祝', '董', '梁']

    first_names = ['的', '一', '是', '了', '我', '不', '人', '在', '他', '有', '这', '个', '上', '们', '来', '到', '时', '大', '地', '为',
                   '子', '中', '你', '说', '生', '国', '年', '着', '就', '那', '和', '要', '她', '出', '也', '得', '里', '后', '自', '以',
                   '乾', '坤', '']

    from concurrent.futures import ThreadPoolExecutor

    pool = ThreadPoolExecutor(max_workers=10)

    from concurrent.futures import ProcessPoolExecutor

    p_pool = ProcessPoolExecutor()

    for u_id in range(10000):
        u = manager.user().read_from_dict({
            'user_id': str(uuid.uuid1()),
            'info_id': str(uuid.uuid1()),
            'user_name': random.choice(last_names) + random.choice(first_names),
            'user_pwd': str(uuid.uuid1()),
            'user_status': 'create',
            'create_time': datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S'),
            'update_time': datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S'),
        })
        # p_pool.submit(u.insert)
        # pool.submit(u.insert)
        # Thread(target=u.insert).start()
        u.insert()


@mark_exe_times
def test_large_select():
    """
    测试大量数据查询

    第一次测试:(耗时: 101.52734398841858 秒 < 1544171146.346679 , 1544171247.874023 >)
    :return:
    """
    manager = init_orm(['user'])
    user = manager.user()
    result = user.select_all()
    for r in result:
        print(r)


@mark_exe_times
def single_manager_test():
    """
    单个orm管理器测试

    第一次测试:(耗时: 1.015625 秒 < 1544171383.680664 , 1544171384.696289 >)
    第二次测试:(耗时: 14.758985996246338 秒 < 1545632684.362042 , 1545632699.121028 >)(577385)
    :return:
    """
    manager = init_orm()
    a = manager.a()
    a.select()


@mark_exe_times
def page_test():
    """
    分页测试

    第一次测试:(耗时: 2.908003091812134 秒 < 1547520742.423657 , 1547520745.33166 >)
    :return:
    """
    manager = init_orm(['user'])
    a = manager.user()
    print(a.limit(0, 1).select())
    print(a.count())
    result = a.page(57738, 10, 3).select()
    print(result)
    result = a.page(5, 10).select()
    print(result)
    for r in result['list']:
        print(r)


if '__main__' == __name__:
    # a = init_orm()
    # b = init_orm(['user'])
    # print(a)
    # print(b)
    # single_manager_test()
    # single_manager_test_home()
    # test_seq_insert()
    # test_large_select()
    page_test()
